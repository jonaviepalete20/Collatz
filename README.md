#include <iostream>
#include <string>
using namespace std;
int main()
{
	int userinput, n, f;
	int collatz[1000];
	int fibonacci[90];
	int a=0;
	int first = 0;
	int second = 1;
	int even = 0;
	
	cout<<"Enter an integer:"<<endl;
	cin>>userinput;
	
	cout<<""<<endl;
	cout<<"Collatz Sequence: "<<endl;
	while(userinput!= 1)
	{
		if (userinput % 2 == 0)
		{
			userinput = userinput/2;
			cout<<userinput<<endl;
			collatz[a] = userinput;
			even++;
		}
		
		else
		{
			userinput = userinput*3 + 1;
			cout<<userinput<<endl;
			collatz[a] = userinput;
		}
		a++;
	}
	
//Custom function. Counting the even number from the collatz sequence
	cout<<"Even number in collatz sequence: ";
	cout<<even<<endl;
	cout<<""<<endl;